window.onload = init;


function init(){
    const map = new ol.Map({
        view: new ol.View({
            center: [1390544.5702751142, 5144706.428910976],
            zoom: 7,
        }),
        target: 'bike-map'
    })

    const openStreetMapStandard = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible:true,
        title: 'OSMStandard'
    })

    const stamenToner = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
        }),
        visible: false,
        title: 'StamenToner'
    })

    const groupLayer = new ol.layer.Group({
        layers: [
            openStreetMapStandard,
            stamenToner,
        ]
    })

    map.addLayer(groupLayer)

    const radioSwitcher = document.querySelectorAll('input[type=radio]')
    for(let radioInput of radioSwitcher){
        radioInput.addEventListener('change', function(){
            let selectedMap = this.value

            groupLayer.getLayers().forEach(element => {
                let levelTitle = element.get('title')
                element.setVisible(levelTitle === selectedMap)
            });
        })
    }
    const romaCampobasso = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/Campobasso.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: false,
        title: 'romaCampobasso',
        style: new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: [20, 20, 20, 0.8],
                width: 12
            })
        })
    })
    const romaCaserta = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/Caserta.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: false,
        title: 'romaCaserta',
        style: new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: [250, 20, 20, 0.8],
                width: 12
            }),
        }) 
    })
    const romaNapoli = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/Napoli.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'romaNapoli',
        style: new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: [100, 20, 20, 0.8],
                width: 12
            }),
        }) 
    })
    const napoli = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/localNapoli.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'napoli',
        style: new ol.style.Style({
            image: new ol.style.Circle({
                radius: 10,
                stroke: new ol.style.Stroke({
                    color: [151, 30, 151, 0.8],
                    width: 6
                }),
            })
        }) 
    })
    const roma = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/localRoma.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'roma',
        style: new ol.style.Style({
            image: new ol.style.Circle({
                radius: 10,
                stroke: new ol.style.Stroke({
                    color: [5, 110, 20, 0.8],
                    width: 3
                }),
            })
        }) 
    })
    const caserta = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/localCaserta.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'caserta',
        style: new ol.style.Style({
            image: new ol.style.Circle({
                radius: 10,
                stroke: new ol.style.Stroke({
                    color: [2, 27, 10, 0.8],
                    width: 6
                }),
            })
        }) 
    })
    const campobasso = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/localCampobasso.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'campobasso',
        style: new ol.style.Style({
            image: new ol.style.Circle({
                radius: 10,
                stroke: new ol.style.Stroke({
                    color: [161, 90, 8, 0.8],
                    width: 6
                }),
            })
        }) 
    })

    const groupBikeMap = new ol.layer.Group({
        layers: [
            romaCampobasso,
            romaCaserta,
            romaNapoli
        ]
    }) 
    const groupPointMap = new ol.layer.Group({
        layers: [
            napoli,
            campobasso,
            caserta,
            roma
        ]
    }) 

    map.addLayer(groupBikeMap)
    map.addLayer(groupPointMap)

    const checkboxSwitcher = document.querySelectorAll('input[type=checkbox]')
    /* console.log(checkboxSwitcher) */

    for(let checkInput of checkboxSwitcher){
        checkInput.addEventListener('change', function(){
            // console.log(checkInput)
            // console.log(checkInput.checked)
            // console.log(checkInput.id)
            groupBikeMap.getLayers().forEach(element => {
                    let levelTitle = element.get('title')
                    if(checkInput.checked){
                        element.setVisible(levelTitle === checkInput.id)
                    }else{
                        element.setVisible(false)
                    }                    
                });          
        })
    }
    const roadSwitcher = document.querySelectorAll('.selettore-ol')
    
    // for(let radioInput of roadSwitcher){
    //     radioInput.addEventListener('change', function(){
    //         let selectedRadio = this.value

    //         groupBikeMap.getLayers().forEach(element => {
    //             let levelTitle = element.get('title')
    //             element.setVisible(levelTitle === selectedRadio)
    //         });
    //     })
    // }
    const overlayHtml = document.querySelector('.overlay-container');
    const overlayLayer = new ol.Overlay({
        element: overlayHtml
    })
    map.addOverlay(overlayLayer)

    const spanOverlayNome = document.getElementById('overlay-nome');
    const spanOverlayInfo = document.getElementById('overlay-info');

    map.on('click', function(evt){
        overlayLayer.setPosition(undefined)

        map.forEachFeatureAtPixel(evt.pixel, function(feature, layer){
            let nome = feature.get('nome')
            let info = feature.get('Descrizione')

            spanOverlayNome.innerHTML = "Direzione "+ nome;
            spanOverlayInfo.innerHTML = info;

            overlayLayer.setPosition(evt.coordinate)
        })
    })

}
